import React from 'react';
import {SafeAreaView,StatusBar,NetInfo} from 'react-native';
import Router from "./routers/Router";

console.disableYellowBox = true;


export let navigatorRef;

export default class App extends React.Component {
    
    constructor(props){
        super(props);
    }
    render(){

        return(
          <SafeAreaView style={{ flex: 1,backgroundColor: '#fff'}}>
            <StatusBar translucent barStyle = "dark-light-content" backgroundColor = {'#000'} />
            
                <Router ref={nav => {this.navigator = nav } }/> 
            
          </SafeAreaView>
        );
    }   
}
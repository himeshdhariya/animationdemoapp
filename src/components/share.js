//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text,TouchableOpacity } from 'react-native';
// import all basic components
import { ShareDialog } from 'react-native-fbsdk';

const FBSDK = require('react-native-fbsdk');
const {
  ShareApi,
} = FBSDK;



export default class Share extends Component {

    constructor(props){
        super(props);
        this.state={
            // sharePhotoContent:''
        }
    }

  render() {  
        const photoUri = 'file://' + '/storage/emulated/0/Pictures/1570707726651.jpg'
    const sharePhotoContent = {
    contentType : 'photo',
    photos: [{ imageUrl: photoUri }],
    }

    return (
      <View style={styles.MainContainer}>
          <TouchableOpacity onPress={this.share.bind(this)}>
            <Text style={{ fontSize: 23 }}> SHARE </Text>
            {ShareDialog.show(this.state.sharePhotoContent)}
          </TouchableOpacity>
        
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    marginTop: 50,
    justifyContent: 'center',
  },
});




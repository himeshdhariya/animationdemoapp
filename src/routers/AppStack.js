import Home from '../components/Home';
import { createStackNavigator } from 'react-navigation-stack';

const AppStack = createStackNavigator({
    Home:Home,
},
{
    headerMode: 'none',
});

export default AppStack;
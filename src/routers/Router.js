import { createAppContainer } from "react-navigation";
import SwitchStack from './SwitchStack';

const Router = createAppContainer(SwitchStack);

export default Router;

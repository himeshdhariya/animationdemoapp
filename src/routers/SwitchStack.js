import { createSwitchNavigator } from 'react-navigation';
import AppStack from './AppStack';

 const SwitchStack = createSwitchNavigator(
    {
        App: AppStack
    },
    {
      initialRouteName: 'App',
    },
);

export default SwitchStack;